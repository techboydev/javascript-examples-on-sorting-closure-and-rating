using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using XmlRpc.Types;
using XmlRpc.Types.Structs;

namespace WorkerService.Models.Liss
{
    public class Student
    {

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString StudentId = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString FirstName = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString Surname = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString PreferredName = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString Form = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString RollGroup = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString House = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString Gender = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString StatewideId = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString Email = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString Phone = new XmlRpcString();

        /// <summary>
        /// 
        /// </summary>
        public XmlRpcString Guid = new XmlRpcString();

        public static Student ParseXml(XElement el)
        {
            var members = el.Descendants("member");
            var student = new Student();

            foreach (var member in members)
            {
                switch (member.Element("name").Value)
                {
                    case "StudentId":
                        student.StudentId.ParseXml(member.Element("value"));
                        break;
                    case "FirstName":
                        student.FirstName.ParseXml(member.Element("value"));
                        break;
                    case "Surname":
                        student.Surname.ParseXml(member.Element("value"));
                        break;
                    case "PreferredName":
                        student.PreferredName.ParseXml(member.Element("value"));
                        break;
                    case "Form":
                        student.Form.ParseXml(member.Element("value"));
                        break;
                    case "RollGroup":
                        student.RollGroup.ParseXml(member.Element("value"));
                        break;
                    case "House":
                        student.House.ParseXml(member.Element("value"));
                        break;
                    case "Gender":
                        student.Gender.ParseXml(member.Element("value"));
                        break;
                    case "StatewideId":
                        student.StatewideId.ParseXml(member.Element("value"));
                        break;
                    case "Email":
                        student.Email.ParseXml(member.Element("value"));
                        break;
                    case "Phone":
                        student.Phone.ParseXml(member.Element("value"));
                        break;
                    case "Guid":
                        student.Guid.ParseXml(member.Element("value"));
                        break;
                    default:
                        throw new Exception("Unknown Member: " + member.Element("name").Value);
                }
            }

            return student;
        }

        public Data.Models.Student ToStudent()
        {
            var student = new Data.Models.Student(); ;
            student.FirstName = FirstName.Value;
            student.Surname = Surname.Value;
            student.YearLevel = Form.Value;
            student.House = House.Value;

            student.Email = Email.Value.ToLower();
            student.ExternalCode = StudentId.Value;
            return student;

        }
    }
}
